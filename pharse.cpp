/* Copyright (C) 2013 Calvin Beck

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 */

/*
  Implementation of functions for phone index parsing.
 */

#include "pharse.h"
#include <stdint.h>


/*
  Obvious helper functions...

 */

static int is_delim(const char c)
{
    /* Using semi-colon as a delimiter */
    return c == ';';
}

static int is_space(const char c)
{
    return c == ' ';
}

static int is_tab(const char c)
{
    return c == '\t';
}

static int is_newline(const char c)
{
    return c == '\n';
}

static int is_white_space(const char c)
{
    return is_space(c) || is_tab(c) || is_newline(c);
}


/*
  Enum for keeping track of the state our parser is in.

 */

typedef enum {
    START_STATE,
    NAME_STATE,
    PHONE_STATE,
    FILE_STATE
} info_state_t;


ContactInfo parse_info(File * const phone_file, pharse_err_t *err)
{
    /* Struct to store our parsed information in */
    ContactInfo info;

    /* State of the parser */
    info_state_t cur_state = START_STATE;

    /* Various indices for the parser */
    size_t name_ind  = 0;
    size_t phone_ind = 0;
    size_t file_ind  = 0;

    /* I don't want a billion if (NULL == err) checks... */
    if (NULL == err) {
	pharse_err_t tmp_err;
	err = &tmp_err;
    }

    /*
      We need to keep track of the number of EOFs that we have
      encountered in a loop - if we run into an EOF twice then the
      file is not to be trusted.

    */
    uint8_t num_eofs = 0;

    while (num_eofs < 2) {
	char read_char = phone_file->peek();

	/* If we have run into the end of the file we need to loop */
	if (read_char == -1) {
	    phone_file->seek(0);
	    num_eofs++;

	    continue;
	}

	switch (cur_state) {

	case START_STATE:
	    /* Starting state - eat whitespace until we get to the data */

	    if (is_white_space(read_char)) {
		phone_file->read();
		continue; /* Skip over starting whitespace */
	    }
	    else {
		/* Starting to read a name, push character back on stream */
		cur_state = NAME_STATE;
		continue;
	    }

	    break;

	case NAME_STATE:
	    /* State for reading the name field */

	    if (is_delim(read_char)) {
		/* Make sure our string is NULL terminated */
		info.name[name_ind] = '\0';

		cur_state = PHONE_STATE; /* On to parsing phone numbers */
	    }
	    else if (name_ind < NAME_LEN - 1) {
		info.name[name_ind] = read_char;
		name_ind++;
	    }

	    break;

	case PHONE_STATE:
	    /* State for reading the phone number field */

	    if (is_delim(read_char)) {
		/* Make sure our string is NULL terminated */
		info.p_num[phone_ind] = '\0';

		cur_state = FILE_STATE;
	    }
	    else if (phone_ind < PHONE_LEN - 1) {
		info.p_num[phone_ind] = read_char;
		phone_ind++;
	    }

	    break;

	case FILE_STATE:
	    /* State for reading the file name field */

	    if (is_newline(read_char)) {
		/* Make sure the file name is NULL terminated */
		info.img_file[file_ind] = '\0';

		/* Parse was successful */
		*err = SUCCESS;
		return info;
	    }
	    else if (file_ind < FILENAME_LEN - 1) {
		info.img_file[file_ind] = read_char;
		file_ind++;

		phone_file->read();
		continue;
	    }
	    else {
		/* Filename too long :C */
		*err = INVALID_FILENAME;
		return info;
	    }
	}

	/* Need to advance the position in the file now */
	phone_file->read();

    }

    /* We have looped through the file - bad file! */
    *err = BAD_FILE;
    return info;
}


static int rewind_record(File * const phone_file)
{
    int num_tabs  = 0;
    int num_loops = 0;

    /* Rewind the file to the previous record */
    while (num_loops < 2) {
	char cur = phone_file->peek();

	if (2 == num_tabs && (is_newline(cur) || phone_file->position() == 0)) {
	    return 0;
	}
	else if (phone_file->position() == 0) {
	    phone_file->seek(phone_file->size() - 1);
	    num_loops++;
	}
	else {
	    if (is_delim(cur)) {
		num_tabs++;
	    }

	    unsigned long pos = phone_file->position();
	    phone_file->seek(pos - 1);
	}
    }

    return 1;
}


ContactInfo parse_info_rev(File * const phone_file, pharse_err_t *err)
{
    rewind_record(phone_file);
    rewind_record(phone_file);

    return parse_info(phone_file, err);
}
