/* Copyright (C) 2013 Calvin Beck

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 */

#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <SD.h>
#include "pharse.h"

/*
  Simple test file for the phone parsing stuff.
 */

// standard U of A library settings, assuming Atmel Mega SPI pins
#define SD_CS    5  // Chip select line for SD card
#define TFT_CS   6  // Chip select line for TFT display
#define TFT_DC   7  // Data/command line for TFT
#define TFT_RST  8  // Reset line for TFT (or connect to +5V)

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

void print_info(ContactInfo const * const info)
{
    Serial.println("--- Contact Information ---");
    Serial.print("Name:  "); Serial.println(info->name);
    Serial.print("Phone: "); Serial.println(info->p_num);
    Serial.print("Pic:   "); Serial.println(info->img_file);

}

void setup()
{
    Serial.begin(9600);

    tft.initR(INITR_REDTAB); 

    Serial.print("Initializing SD card...");

    if (!SD.begin(SD_CS)) {
	Serial.println("failed!");
	return;
    }

    Serial.println("Okay.");

    File phone_file;

    /* Open the file */
    if ((phone_file = SD.open("contacts.txt")) == NULL) {
	Serial.println("File not found!");
	return;
    }

    pharse_err_t err;
    ContactInfo record = parse_info(&phone_file, &err);

    Serial.println("Entering loop...");
    for (int i = 0; i < 10; i++) {
	record = parse_info_rev(&phone_file, &err);
	print_info(&record);

    }

}

void loop() {}
