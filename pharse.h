/* Copyright (C) 2013 Calvin Beck

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 */

/*
  Phone index parsing structures and function definitions.

 */

#include <stdio.h>
#include <stddef.h>
#include <SD.h>

#ifndef PHARSE_H_
#define PHARSE_H_


const size_t NAME_LEN     = 32;  /* Maximum length of a person's name */
const size_t PHONE_LEN    = 16;   /* Maximum length of a phone number */
const size_t FILENAME_LEN = 256;  /* Maximum length of an image file name */


typedef enum {
    SUCCESS = 0,
    INVALID_NAME,
    INVALID_PHONE,
    INVALID_FILENAME,
    BAD_FILE
} pharse_err_t;


/*
  A structure for a single person's contact information. This just
  groups together a persons name, phone number, as well as the file
  name of their image (each person will have a contact image).

  Fields:
     name: The name of the person that the number belongs to.

     p_num: The phone number of the person stored as a string.

     img_file: The path to the person's image file.

 */

typedef struct ContactInfo {
    char name[NAME_LEN];
    char p_num[PHONE_LEN];
    char img_file[FILENAME_LEN];
} ContactInfo;


/*
  Read in a single ContactInfo structure from a file. This file should
  have the format:

      <name>;<phone-number>;<contact-img-file>

  This function does not verify the validity of the phone number field
  at all. Nor does it verify the correctness the correctness of the
  file name (though it will make sure it fits in 256 bytes).

  Arguments:
     phone_file: Pointer to the file that we are working on.

     pharse_err_t *err: Pointer to a location to store an error code.

  This function will change the file position indicator, and assumes
  that the file position indicator has not been changed by other
  functions.

 */

ContactInfo parse_info(File * const phone_file, pharse_err_t *err);


/*
  Read in a previous ContactInfo structure from a file. This file
  should have the format:

      <name>;<phone-number>;<contact-img-file>

  This function does not verify the validity of the phone number field
  at all. Nor does it verify the correctness the correctness of the
  file name (though it will make sure it fits in 256 bytes).

  Arguments:
     phone_file: Pointer to the file that we are working on.

     pharse_err_t *err: Pointer to a location to store an error code.

  This function will change the file position indicator, and assumes
  that the file position indicator has not been changed by other
  functions.

  Note: You should probably call parse_info() at least once before
  this - The function works by rewinding from the current file
  position and it assumes that you have just read the current
  ContactInfo structure, so it expects the file position to be
  immediately after that set of contact information. When you load the
  file initially the file position will be before it.

 */

ContactInfo parse_info_rev(File * const phone_file, pharse_err_t *err);

#endif
